let renderRadioButton = (data) => {
  return `<div class="form-check">
        <label class="form-check-label">
        <input type="radio" class="form-check-input" name="singleChoice" value=${data.exact} value="checkedValue" >
        ${data.content}
      </label>
    </div>`;
};

let renderSingleChoice = (question) => {
  let contentAnswer = "";
  question.answers.forEach((item) => {
    let radioHTML = renderRadioButton(item);
    contentAnswer += radioHTML;
  });
  return `<p>${question.content}</p>
  
  <div>
    ${contentAnswer}
  </div>`;
};

const renderFillToInput = (question) => {
  return `<div class="form-group">
    <label for="">${question.content}</label>
    <input type="text" data-no-answer="${question.answers[0].content}" class="form-control" name="" id="fillInput" placeholder="">
  </div>`;
};
// Export sang file main.js để sử dụng
export const checkSingleChoice = () => {
  let inputEl = document.querySelector('input[name="singleChoice"]:checked');
  // kiểm tra kết quả của đáp án vừa chọn là đúng hay sai hay ng dùng ko chọn--null
  if (inputEl == null || inputEl.value == "false") {
    return false;
  }
  if (inputEl.value == "true") {
    return true;
  }
};

export const checkFillInput = () => {
  let inputEl = document.getElementById("fillInput");
  console.log("inputEl.dataset: ", inputEl.dataset);

  if (inputEl.value == inputEl.dataset.noAnswer) {
    return true;
  } else {
    return false;
  }
};

export const renderQuestion = (question) => {
  if (question.questionType == 1) {
    //render singleChoice
    document.getElementById("contentQuiz").innerHTML =
      renderSingleChoice(question);
  } else {
    //render fillInput
    document.getElementById("contentQuiz").innerHTML =
      renderFillToInput(question);
  }
};

export const showResult = (list) => {
  // Lấy kết quả trả về khi ng dùng đến câu 8/8
  document.getElementById("endQuiz").classList.remove("d-none");
  document.getElementById("startQuiz").style.display = "none";

  // Đếm số câu trả lời đúng - sai
  let count = 0;
  list.forEach((item) => {
    //   if (item.isCorrect) {
    //     count++;
    //   }
    item.isCorrect && count++;
  });
  document.getElementById("correct").innerHTML = count;
  document.getElementById("incorrect").innerHTML = list.length - count;
};
