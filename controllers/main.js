import { dataQuestion as dataRaw } from "../data/questions.js";
import {
  renderQuestion,
  showResult,
  checkSingleChoice,
  checkFillInput,
} from "./controller.js";

let currentQuestionIndex = 0;
console.table(dataRaw);
// tạo thêm 1 mảng mới là dataRaw để update thêm 1 trường dữ liệu isCorrect
// dataRaw đc lưu dưới mảng tên là dataQuestion để ko cần đi định nghĩa lại
let dataQuestion = dataRaw.map((item) => {
  return { ...item, isCorrect: null };
});

// render lần đầu
document.getElementById("currentStep").innerHTML = `${
  currentQuestionIndex + 1
}/${dataQuestion.length}`;
renderQuestion(dataQuestion[currentQuestionIndex]);

// Khi user nhấn next
function nextQuestion() {
  //kiểm tra đáp án user chọn
  if (dataQuestion[currentQuestionIndex].questionType == 1) {
    // chạy checkSingleChoice
    dataQuestion[currentQuestionIndex].isCorrect = checkSingleChoice();
    console.table(dataQuestion);
  } else {
    dataQuestion[currentQuestionIndex].isCorrect = checkFillInput();
    console.table(dataQuestion);
  }

  currentQuestionIndex++;
  // kiểm tra số lượng câu hỏi hiện đang hiển thị
  // nếu hiển thị đủ số lượng câu
  if (currentQuestionIndex == dataQuestion.length) {
    showResult(dataQuestion);
    return; // dừng chương trình
  }

  document.getElementById("currentStep").innerHTML = `${
    currentQuestionIndex + 1
  }/${dataQuestion.length}`;

  //render nội dung câu hỏi
  renderQuestion(dataQuestion[currentQuestionIndex]);
}

window.nextQuestion = nextQuestion;
